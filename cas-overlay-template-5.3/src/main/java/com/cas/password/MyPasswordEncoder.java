package com.cas.password;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * 密码加密
 */
public class MyPasswordEncoder implements PasswordEncoder {

    @Override
    public String encode(CharSequence charSequence) {
        System.out.println("encode===========用户输入的密码:" + charSequence);
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder.encode(charSequence);
    }

    @Override
    public boolean matches(CharSequence charSequence, String str) {
        //charSequence 为用户输入的密码
        System.out.println("matches---------------用户输入的密码:"+charSequence);
        //str 为数据库密码
        System.out.println("matches---------------数据库密码:"+str);
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder.matches(charSequence, str);
    }
}
